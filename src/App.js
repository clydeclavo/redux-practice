import React from 'react';
import './App.css';
import { Provider } from 'react-redux';
import store from './store';
import Todos from './components/Todos'
import AddTodo from './components/AddTodo';

function App() {
  return (
    <Provider store = {store}>
      <div className="App">
        <header className="App-header">
          <h3>Todo List</h3>
        </header>
        <div>
           <AddTodo />
           <Todos />
        </div>
      </div>
    </Provider>
  );
}

export default App;
