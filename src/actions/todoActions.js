import { GET_TODOS, TOGGLE_TODO, ADD_TODO, REMOVE_TODO } from './types';

export const getTodos = () => ({
  type: GET_TODOS
});

export const toggleTodo = index => ({
  type: TOGGLE_TODO,
  index
});

export const addTodo = title => ({
  type: ADD_TODO,
  title
});

export const removeTodo = index => ({
  type: REMOVE_TODO,
  index
});
