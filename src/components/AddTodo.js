import React from 'react';
import { addTodo } from '../actions/todoActions';
import { useDispatch } from 'react-redux';

export default function AddTodo() {
  const dispatch = useDispatch();

  const onSubmit = e => {
    e.preventDefault();
    if (!input.value.trim()) {
      return;
    }
    dispatch(addTodo(input.value));
    input.value = '';
    console.log('Submitting');
  };

  let input;

  return (
    <div>
      <form onSubmit={onSubmit}>
        <input
          type="text"
          name="newTodo"
          className="text-box"
          autoComplete="off"
          placeholder="what should you do?"
          ref={node => (input = node)}
        />

        <input type="submit" value="Add" className="btn" />
      </form>
    </div>
  );
}
