import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { toggleTodo, removeTodo } from '../actions/todoActions';

export default function Todos() {
  const todos = useSelector(state => state.todos);
  const dispatch = useDispatch();

  const todosItems = todos.map((todo, index) => (
    <div key={todo.id} className={`todo-item ${todo.completed ? 'todo-done' : ''}`}>
      <label>
        <input
          checked={todo.completed}
          type="checkbox"
          onChange={() => dispatch(toggleTodo(index))}
        />
        {todo.title}
        <button className="remove-btn" onClick={() => dispatch(removeTodo(index))}>
          Remove
        </button>
      </label>
    </div>
  ));
  return <div className="todo-list">{todosItems}</div>;
}
