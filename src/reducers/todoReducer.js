import { GET_TODOS, TOGGLE_TODO, ADD_TODO, REMOVE_TODO } from '../actions/types';

const initialState = [
  {
    id: 1,
    title: 'Learn Redux',
    completed: false
  },
  {
    id: 2,
    title: 'Learn React',
    completed: true
  }
];

export const todoReducer = function(state = initialState, action) {
  switch (action.type) {
    case GET_TODOS:
      return state;

    case TOGGLE_TODO:
      return state.map((todo, index) => {
        if (index === action.index) {
          return Object.assign({}, todo, {
            completed: !todo.completed
          });
        }
        return todo;
      });

    case ADD_TODO:
      return [
        ...state,
        {
          id: state.length + 1,
          title: action.title,
          completed: false
        }
      ];

    case REMOVE_TODO:
      return state.filter((todo, index, state) => index !== action.index);

    default:
      return state;
  }
};
